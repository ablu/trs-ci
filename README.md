Latest daily build:
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
          <img src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?passrate" />
        </a>


<!--
  Gitlab allows certain HTML tags to be added as Markdown.
  The full list of allowed tags is here: https://github.com/gjtorikian/html-pipeline/blob/a363d620eba0076479faad86f0cf85a56b2b3c0a/lib/html/pipeline/sanitization_filter.rb
  Also note that there cannot be spaces between tags, otherwise HTML parser doesn't work
-->

<table>
  <thead>
    <th></th>
    <th>avadp <sup>EWAOL</sup></th>
    <th>avadp <sup>TRS (Xen)</sup></th>
    <th>qemu <sup>TRS</sup></th>
    <th>rockpi4b <sup>TRS</sup></th>
    <th>rpi4 <sup>TRS</sup></th>
    <th>synquacer <sup>TRS</sup></th>
    <th>kv260 starter kit <sup>TRS</sup></th>
    <th>zynqmp-zcu102 <sup>TRS</sup></th>
  <thead>
  <tbody>
    <!-- build suite -->
    <tr>
      <th>build</th>
      <!-- avadp -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=ava&suite=build">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=ava&suite=build&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- avadp-xen -->
      <td>
        N/A
      </td>
      <!-- qemu -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=qemu&suite=build">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=qemu&suite=build&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- rk3399-rock-pi-4b -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=rk3399-rock-pi-4b&suite=build">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=rk3399-rock-pi-4b&suite=build&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- rpi4 -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=rpi4&suite=build">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=rpi4&suite=build&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- synquacer -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=synquacer&suite=build">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=synquacer&suite=build&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- zynqmp-kria-starter -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=kv260&suite=build">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=kv260&suite=build&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- zynqmp-zcu102 -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=zcu102&suite=build">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=zcu102&suite=build&passrate&title&hide_zeros=1" />
        </a>
      </td>
    </tr>
    <!-- end of build suite -->
    <!-- boot suite (smoke-ls) -->
    <tr>
      <th>boot</th>
      <!-- avadp -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=ava&suite=smoke-ls">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=ava&suite=smoke-ls&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- avadp-xen -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=ava-xen&suite=smoke-ls">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=ava-xen&suite=smoke-ls&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- qemu -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=qemu&suite=smoke-ls">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=qemu&suite=smoke-ls&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- rk3399-rock-pi-4b -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=rk3399-rock-pi-4b&suite=smoke-ls">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=rk3399-rock-pi-4b&suite=smoke-ls&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- rpi4 -->
      <td>
        N/A
      </td>
      <!-- synquacer -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=synquacer&suite=smoke-ls">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=synquacer&suite=smoke-ls&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- kv260 -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=kv260&suite=smoke-ls">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=kv260&suite=smoke-ls&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- zynqmp-zcu102 -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=zcu102&suite=smoke-ls">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=zcu102&suite=smoke-ls&passrate&title&hide_zeros=1" />
        </a>
      </td>
    </tr>
    <!-- end of boot suite -->
    <!-- filesystem-encryption suite -->
    <tr>
      <th>filesystem encryption</th>
      <!-- avadp -->
      <td>
        N/A
      </td>
      <!-- avadp-xen -->
      <td>
        N/A
      </td>
      <!-- qemu -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=qemu&suite=filesystem-encryption">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=qemu&suite=filesystem-encryption&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- rk3399-rock-pi-4b -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=rk3399-rock-pi-4b&suite=filesystem-encryption">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=rk3399-rock-pi-4b&suite=filesystem-encryption&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- rpi4 -->
      <td>
        N/A
      </td>
      <!-- synquacer -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=synquacer&suite=filesystem-encryption">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=synquacer&suite=filesystem-encryption&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- kv260 -->
      <td>
        N/A
      </td>
      <!-- zynqmp-zcu102 -->
      <td>
        N/A
      </td>
    </tr>
    <!-- end of filesystem-encryption suite -->
    <!-- measured-boot suite -->
    <tr>
      <th>measured boot</th>
      <!-- avadp -->
      <td>
        N/A
      </td>
      <!-- avadp-xen -->
      <td>
        N/A
      </td>
      <!-- qemu -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=qemu&suite=measured-boot">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=qemu&suite=measured-boot&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- rk3399-rock-pi-4b -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=rk3399-rock-pi-4b&suite=measured-boot">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=rk3399-rock-pi-4b&suite=measured-boot&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- rpi4 -->
      <td>
        N/A
      </td>
      <!-- synquacer -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=synquacer&suite=measured-boot">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=synquacer&suite=measured-boot&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- kv260 -->
      <td>
        N/A
      </td>
      <!-- zynqmp-zcu102 -->
      <td>
        N/A
      </td>
    </tr>
    <!-- end of measured-boot suite -->
    <!-- xtest suite -->
    <tr>
      <th>optee-xtest</th>
      <!-- avadp -->
      <td>
        N/A
      </td>
      <!-- avadp-xen -->
      <td>
        N/A
      </td>
      <!-- qemu -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=qemu&suite=xtest">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=qemu&suite=xtest&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- rk3399-rock-pi-4b -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=rk3399-rock-pi-4b&suite=xtest">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=rk3399-rock-pi-4b&suite=xtest&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- rpi4 -->
      <td>
        N/A
      </td>
      <!-- synquacer -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=synquacer&suite=xtest">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=synquacer&suite=xtest&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- kv260 -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=kv260&suite=xtest">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=kv260&suite=xtest&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- zynqmp-zcu102 -->
      <td>
        N/A
      </td>
    </tr>
    <!-- end of xtest suite -->
    <!-- secure-boot suite -->
    <tr>
      <th>secure boot</th>
      <!-- avadp -->
      <td>
        N/A
      </td>
      <!-- avadp-xen -->
      <td>
        N/A
      </td>
      <!-- qemu -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=qemu&suite=secure-boot-enabled">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=qemu&suite=secure-boot-enabled&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- rk3399-rock-pi-4b -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=rk3399-rock-pi-4b&suite=secure-boot-enabled">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=rk3399-rock-pi-4b&suite=secure-boot-enabled&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- rpi4 -->
      <td>
        N/A
      </td>
      <!-- synquacer -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=synquacer&suite=secure-boot-enabled">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=synquacer&suite=secure-boot-enabled&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- kv260 -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=kv260&suite=secure-boot-enabled">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=kv260&suite=secure-boot-enabled&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- zynqmp-zcu102 -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=zcu102&suite=secure-boot-enabled">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=zcu102&suite=secure-boot-enabled&passrate&title&hide_zeros=1" />
        </a>
      </td>
    </tr>
    <!-- end of secure-boot suite -->
    <!-- soafee suite -->
    <tr>
      <th>soafee</th>
      <!-- avadp -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=ava&suite=soafee-test-suite">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=ava&suite=soafee-test-suite&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- avadp-xen -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=ava-xen&suite=soafee-test-suite">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=ava-xen&suite=soafee-test-suite&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- qemu -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=qemu&suite=soafee-test-suite">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=qemu&suite=soafee-test-suite&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- rk3399-rock-pi-4b -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=rk3399-rock-pi-4b&suite=soafee-test-suite">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=rk3399-rock-pi-4b&suite=soafee-test-suite&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- rpi4 -->
      <td>
        N/A
      </td>
      <!-- synquacer -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=synquacer&suite=soafee-test-suite">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=synquacer&suite=soafee-test-suite&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- kv260 -->
      <td>
        N/A
      </td>
      <!-- zynqmp-zcu102 -->
      <td>
        N/A
      </td>
    </tr>
    <!-- end of soafee suite -->
    <!-- capsule updates -->
    <tr>
      <th>capsule updates</th>
      <!-- avadp -->
      <td>
	N/A
      </td>
      <!-- avadp-xen -->
      <td>
	N/A
      </td>
      <!-- qemu -->
      <td>
	N/A
      </td>
      <!-- rk3399-rock-pi-4b -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=rk3399-rock-pi-4b&suite=capsule-updates">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=rk3399-rock-pi-4b&suite=capsule-updates&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- rpi4 -->
      <td>
        N/A
      </td>
      <!-- synquacer -->
      <td>
        N/A
      </td>
      <!-- kv260 -->
      <td>
        N/A
      </td>
      <!-- zynqmp-zcu102 -->
      <td>
        N/A
      </td>
    </tr>
    <!-- capsule updates -->
    <!-- smart-camera -->
    <tr>
      <th>smart-camera</th>
      <!-- avadp -->
      <td>
	N/A
      </td>
      <!-- avadp-xen -->
      <td>
	N/A
      </td>
      <!-- qemu -->
      <td>
	N/A
      </td>
      <!-- rk3399-rock-pi-4b -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/sdc/build/latest-finished/tests?environment=rockpi&suite=sdc-test">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/sdc/build/latest-finished/badge?environment=rockpi&suite=sdc-test&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- rpi4 -->
      <td>
        N/A
      </td>
      <!-- synquacer -->
      <td>
        N/A
      </td>
      <!-- kv260 -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/sdc/build/latest-finished/tests?environment=kv260&suite=sdc-test">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/sdc/build/latest-finished/badge?environment=kv260&suite=sdc-test&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- zynqmp-zcu102 -->
      <td>
        N/A
      </td>
    </tr>
    <!-- smart-camera -->
  </tbody>
</table>


Legend:


* <img src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=donotexist&suite=donotexist&passrate&title&hide_zeros=1" /> : No results yet, tests are in progress

# ARM Blueprints CI Images

This CI is intended to build and test the following projects (links point to the latest successful build for each target):
- [![pipeline status](https://gitlab.com/Linaro/trustedsubstrate/meta-ts/badges/master/pipeline.svg)](https://gitlab.com/Linaro/trustedsubstrate/meta-ts/-/commits/master) meta-ts (Trusted Substrate): https://gitlab.com/Linaro/trustedsubstrate/meta-ts
  * Download images:
    * qemu
        * [flash.bin-qemu.gz](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/flash.bin-qemu.gz?job=build-meta-ts-qemuarm64-secureboot)
    * rockpi
        * [ts-firmware-rockpi4b.rootfs.wic.gz](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/ts-firmware-rockpi4b.rootfs.wic.gz?job=build-meta-ts-rockpi4b)
    * raspberry pi
        * [ts-firmware-rpi4.rootfs.wic.gz](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/ts-firmware-rpi4.rootfs.wic.gz?job=build-meta-ts-rpi4)
    * synquacer
        * [fip.bin-synquacer.gz](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/fip.bin-synquacer.gz?job=build-meta-ts-synquacer)
        * [scp_romramfw_Release.bin.gz](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/scp_romramfw_Release.bin.gz?job=build-meta-ts-synquacer)
    * zynqmp-kria-starter
        * [ImageA.bin.gz](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/ImageA.bin.gz?job=build-meta-ts-zynqmp-kria-starter)
        * [ImageB.bin.gz](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/ImageB.bin.gz?job=build-meta-ts-zynqmp-kria-starter)
    * zynqmp-zcu102
        * [ts-firmware-zynqmp-zcu102.rootfs.wic.gz](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/ts-firmware-zynqmp-zcu102.rootfs.wic.gz?job=build-meta-ts-zynqmp-zcu102)

- [![pipeline status](https://gitlab.com/Linaro/trusted-reference-stack/trs/badges/main/pipeline.svg)](https://gitlab.com/Linaro/trusted-reference-stack/trs/-/commits/main) meta-trs (Trusted Reference Stack): https://gitlab.com/Linaro/trusted-reference-stack/trs
  * Download images:
    * qemu
        * [trs-image-trs-qemuarm64.rootfs.wic.gz](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/trs-image-trs-qemuarm64.rootfs.wic.gz?job=build-meta-trs)

- [![pipeline status](https://gitlab.com/soafee/ewaol/meta-ewaol-machine/badges/kirkstone-dev/pipeline.svg)](https://gitlab.com/soafee/ewaol/meta-ewaol-machine/-/commits/main) meta-ewaol-machine: https://gitlab.com/soafee/ewaol/meta-ewaol-machine
  * Download images:
    * avadp
        * [ewaol-baremetal-image-ava.rootfs.wic.bz2](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/ewaol-baremetal-image-ava.rootfs.wic.bz2?job=build-meta-ewaol-machine-avadp)

Here's a comparison table listing all targets for EWAOL, TS and TRS side-by-side:

|                      | EWAOL    | TS       | TRS      |
|----------------------|----------|----------|----------|
| avadp                | &#10004; |          |          |
| qemuarm64            |          | &#10004; | &#10004; |
| rockpi4b             |          | &#10004; |          |
| rpi4                 |          | &#10004; |          |
| synquacer            |          | &#10004; |          |
| zynqmp-kria-starter  |          | &#10004; |          |
| zynqmp-zcu102        |          | &#10004; |          |

# Build strategy

There will be builds:
* Everyday at 5pm UTC (scheduled pipeline in https://gitlab.com/Linaro/blueprints/nightly-builds)
* On every:
  * push to the main branch (*main branch needs to be protected*)
  * new merge requests
  * new tags
* On these repositories
  * [![pipeline status](https://gitlab.com/soafee/ewaol/meta-ewaol-machine/badges/kirkstone-dev/pipeline.svg)](https://gitlab.com/soafee/ewaol/meta-ewaol-machine/-/commits/kirkstone-dev) https://gitlab.com/soafee/ewaol/meta-ewaol-machine
  * [![pipeline status](https://gitlab.com/soafee/ewaol/meta-ewaol/badges/kirkstone-dev/pipeline.svg)](https://gitlab.com/soafee/ewaol/meta-ewaol/-/commits/kirkstone-dev) https://gitlab.com/soafee/ewaol/meta-ewaol
  * [![pipeline status](https://gitlab.com/Linaro/trusted-reference-stack/trs/badges/main/pipeline.svg)](https://gitlab.com/Linaro/trusted-reference-stack/trs/-/commits/main) https://gitlab.com/Linaro/trusted-reference-stack/trs
  * [![pipeline status](https://gitlab.com/Linaro/trusted-reference-stack/trs-manifest/badges/main/pipeline.svg)](https://gitlab.com/Linaro/trusted-reference-stack/trs-manifest/-/commits/main) https://gitlab.com/Linaro/trusted-reference-stack/trs-manifest
  * [![pipeline status](https://gitlab.com/Linaro/trustedsubstrate/meta-ts/badges/master/pipeline.svg)](https://gitlab.com/Linaro/trustedsubstrate/meta-ts/-/commits/master) https://gitlab.com/Linaro/trustedsubstrate/meta-ts
  * [![pipeline status](https://gitlab.com/Linaro/trustedsubstrate/meta-ledge-secure/badges/kirkstone/pipeline.svg)](https://gitlab.com/Linaro/trustedsubstrate/meta-ledge-secure/-/commits/kirkstone) https://gitlab.com/Linaro/trustedsubstrate/meta-ledge-secure

Git urls for all components are described in [default.xml](https://gitlab.com/Linaro/trusted-reference-stack/trs-manifest/-/blob/main/default.xml).

## Pipelines

There are 3 types of events triggering pipelines:
* push to the main branch (*main branch needs to be protected*)
* new merge requests
* new tags

**NOTE:** No pipelines are triggered for non-main branches because merge requests originating from branches in the same repository would trigger 2 pipelines on the same change. Gitlab does not have a mechanism to prevent redundant pipelines.

### Push to main branch / new tags
Pipelines for these events take the git url and main branch (or new tag) of the component and change those in the manifest before triggering builds.

### Merge Requests

Because we are an open source project, running Gitlab's open source project, we are not allowed to have merge requests of forked repositories to trigger pipelines in parent projects.
For example, if Bob forks `meta-ts` and open a MR in `meta-ts` from Bob's fork, no pipeline is triggered. This is only available in [Gitlab's premium tier](https://docs.gitlab.com/ee/ci/pipelines/merge_request_pipelines.html#run-pipelines-in-the-parent-project). There is a hidden feature though: if Bob is a member in the parent project, Gitlab will run his pipeline on the parent project's resources.


# The builder: TuxSuite
All builds are actually run by a Linaro service called TuxSuite (tuxsuite.com). It is a newly created project initially developed to build Linux kernels at scale, abstracting all complexity related to disk space and parallel jobs. TuxSuite is in active development, and new features are added often. One of their latest added feature is the ability to re-use the same expertise learned in building kernels at scale to build OpenEmbedded projects.

One can visit their docs page (https://docs.tuxsuite.com) for further explanation of how it works. Below is an example of how we use TuxSuite to build Blueprints projects.


## How to build
Everything in TuxSuite happens in the cloud, so the only thing we need is a way to give it information about our build: that is called a "plan". Here is a working example of a plan:


```yaml
version: 1
name: "build-meta-ts-qemuarm64"
description: "Plan to Build TrustedSubstrate for Qemu"
jobs:
- name: build-meta-ts-qemuarm64
  bakes:
    - sources:
        kas:
          url: "https://gitlab.com/Linaro/trustedsubstrate/meta-ts.git"
          yaml: "ci/qemuarm64-secureboot.yml"
      name: meta-ts-secureboot
```

Save that into `plan.yml`, then run:

```bash
$ tuxsuite plan plan.yml
```

And wait for tuxsuite's magic:

```
Running Bake plan 'build-meta-ts-qemuarm64': 'Plan to Build TrustedSubstrate for Qemu'
Plan https://tuxapi.tuxsuite.com/v1/groups/blueprints/projects/trustedsubstrate/plans/2EAPthrX9oQnufe41igtEEm1dfV

uid: 2EAPthrX9oQnufe41igtEEm1dfV
⚙️  Provisioning: {'kas': {'url': 'https://gitlab.com/Linaro/trustedsubstrate/meta-ts.git', 'yaml': 'ci/qemuarm64-secureboot.yml'}} with None None for None: https://storage.tuxsuite.com/public/blueprints/trustedsubstrate/oebuilds/2EAPtsGSKBSa784LrKVSkFzOaWK/
🚀 Running: {'kas': {'url': 'https://gitlab.com/Linaro/trustedsubstrate/meta-ts.git', 'yaml': 'ci/qemuarm64-secureboot.yml'}} with None None for None: https://storage.tuxsuite.com/public/blueprints/trustedsubstrate/oebuilds/2EAPtsGSKBSa784LrKVSkFzOaWK/
🎉 Pass: {'kas': {'url': 'https://gitlab.com/Linaro/trustedsubstrate/meta-ts.git', 'yaml': 'ci/qemuarm64-secureboot.yml'}} with None None for None: https://storage.tuxsuite.com/public/blueprints/trustedsubstrate/oebuilds/2EAPtsGSKBSa784LrKVSkFzOaWK/

Summary: https://tuxapi.tuxsuite.com/v1/groups/blueprints/projects/trustedsubstrate/plans/2EAPthrX9oQnufe41igtEEm1dfV
2EAPtsGSKBSa784LrKVSkFzOaWK 🎉 Pass with container: ubuntu-20.04 machine: None targets: None
```

Yay! Now head to `https://storage.tuxsuite.com/<your-instance>` and browse through generated files.

**Note**: That actually built an whole OpenEmbedded image in the cloud, without you having to clean up your hard drive or restart the build due to connection issues. Because TuxSuite is hosted in AWS (Amazon Web Services), it is most certainly it will have a steady connection.

# How does it work?

This repository is meant to contain necessary CI scripts to be used by components elsewhere. Gitlab allows repositories point to CI scripts from external files. Therefore, there are 4 main files in this repository: `meta-ts.yml`, `meta-ewaol.yml`, `meta-ewaol-machine.yml` and `nightly-builds.yml`. Each of these are meant to be referenced within **Settings > CI/CD > General Pipelines** configuration in [linaro/trustedsubstrate/meta-ts.git](https://gitlab.com/Linaro/trustedsubstrate/meta-ts), [soafee/ewaol/meta-ewaol.git](https://gitlab.com/soafee/ewaol/meta-ewaol), [soafee/ewaol/meta-ewaol-machine.git](https://gitlab.com/soafee/ewaol/meta-ewaol-machine) and [linaro/blueprints/nightly-builds.git](https://gitlab.com/Linaro/blueprints/nightly-builds/), respectively.

```mermaid
graph TB
  sub1 --> linaro/trustedsubstrate/meta-ts
  sub2 --> linaro/trustedreferencestack/trs
  sub2 --> linaro/trustedreferencestack/trs-manifest
  sub2 --> soafee/ewaol/meta-ewaol
  sub2 --> linaro/trustedsubstrate/meta-ledge-secure
  sub3 --> soafee/ewaol/meta-ewaol-machine
  sub4 --> linaro/blueprints/nightly-builds

  subgraph "linaro/blueprints/ci"
    sub1[meta-ts.yml]
    sub2[meta-trs.yml]
    sub3[meta-ewaol-machine.yml]
    sub4[nightly-builds.yml]

end
```

Then Gitlab will trigger new pipelines in each respective repository.
https://gitlab.com/Linaro/trustedsubstrate/meta-ledge-secure/-/pipelines
**NOTE:** the main reason why there's a separate repository for nightly builds is that we can also run tests for CI itself. Doing both meta-ts/meta-ewaol and CI tests in the same repository became too troublesome and we decided to separate them.

## Subsystems integration

Here's an overview of how CI works along supporting subsystems (SQUAD, LAVA and TuxSuite):

```mermaid
sequenceDiagram
    participant user as Developer
    participant gitlab as Gitlab
    participant squad as SQUAD
    participant lava as LAVA
    participant tuxsuite as TuxSuite

    user->>+gitlab: git-push or MR
    gitlab->>tuxsuite: send build request
    tuxsuite-->>gitlab: build OK
    gitlab->>tuxsuite: download image
    tuxsuite-->>gitlab: image contents
    gitlab->>squad: submit build results
    squad-->>gitlab: build results submitted OK
    alt boot & test
        gitlab->>squad: submit test-job definition
        squad->>lava: submit test-job request
        lava->>gitlab: download OS and FIRMWARE
        gitlab-->>lava: OS and FIRMWARE contents
        lava-->>squad: test-job OK
	squad->>gitlab: trigger check job
        gitlab-->>squad: fetch test results
    end
```

It all starts with a `git-push` or a new merge request event. Gitlab parses the ci script for the repository and triggers a new pipeline. Gitlab then invokes `tuxsuite` cli which sends a build request to [TuxSuite](https://tuxsuite.com) service. After TuxSuite finishes building the requested image, Gitlab downloads it and store inside the job's artifact folder. Next step is to tell [SQUAD](https://qa-reports.linaro.org) that the build passed (or failed).

If there are target boards in [LAVA](https://ledge.validation.linaro.org), boot & test jobs are triggered. Gitlab generates a test-job definition, which is LAVA's specification on how to boot and test things, and use SQUAD to send it to LAVA. After LAVA downloads both OS and FIRMWARE from Gitlab, it triggers boot and tests that are specified in the test-job definition. LAVA then signals SQUAD that job is finished and results are finally fetched by SQUAD. When SQUAD finishes fetching all test results for a pipeline, it will trigger a job in Gitlab that will fetch test results from SQUAD, signaling developers that tests passed or failed.

The main reason why test-job definitions are sent to LAVA via SQUAD is so that SQUAD can collect results of all LAVA runs. And SQUAD does it nicely enough that allows users to be notified when tests fail or regress.

## How to test the BP CI changes locally

From a development standpoint, sometimes you want to test and debug changes locally before submitting a merge/pull request.

It requires to install `jinja2` and `squad-client` using `pip`.

Export `SQUAD_HOST` and `SQUAD_TOKEN` variables before run `./blueprints_ci_tests/run-self-tests.sh`

### Updating the docker image

By default, the CI docker image will be updated whenever there is a push to the Dockerfile in the main branch.
If no changes to Dockerfile are made but its internals require update, one can manually trigger a docker image
update by manually triggering a pipeline and setting a variable named `UPDATE_CI_DOCKER_IMAGE=1`.
