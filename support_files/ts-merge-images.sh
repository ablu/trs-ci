#!/bin/bash

# Merge image_src into image_dst

# Exit when simple command fails
set -o errexit # set -e

# Exit on error inside any functions or subshells
set -o errtrace

# Trigger error when expanding unset variables
set -o nounset # set -u

# Do not hide errors within pipes
set -o pipefail

# Check that two image files are passed as command-line arguments
if [ $# -ne 2 ]; then
  echo "Usage: $0 <image_dst> <image_src>"
  exit 1
fi

# Define image file names
image_dst="$1"
image_src="$2"

export LANG=C

# Get sector size of both images
sector_size=$(fdisk -l "$image_dst" | awk '/Sector size/ {print $4}')

# Get size of image_src in sectors
image_src_size=$(fdisk -l "$image_src" | grep -E "^${image_src}[0-9]+" |
  awk '{print $4}' | awk '{sum+=$1} END {print sum}')

# Get usable destination image last sector
image_dst_end_sector=$(fdisk -l "$image_dst" | grep -E "^${image_dst}[0-9]+" |
  awk 'END{print $3}')

# Get last destination partition number
image_dst_last_part=$(sgdisk -p "$image_dst" | grep -P "^\s+[0-9]+" |
  awk 'END{print $1}')

part_start=$((image_dst_end_sector + 1))

# new size = source image size + 10 MB at the end
new_size=$(((image_src_size) * sector_size + 10 * 1024 * 1024))
truncate --size +${new_size} "$image_dst"

# fix image after resize with sgdisk + fdisk
sgdisk -e "$image_dst"

# Get the partition information for image_src
partinfo=$(sgdisk -p "$image_src")

# Loop through each partition in image_src and create the equivalent partition in
# image_dst
while read -r line; do
  if [[ $line =~ ^[0-9]+ ]]; then
    part_num=$(echo "$line" | awk '{print $1}')
    part_num=$((image_dst_last_part + part_num))

    part_start_src=$(echo "$line" | awk '{print $2}')
    part_end_src=$(echo "$line" | awk '{print $3}')
    part_size_src=$((part_end_src - part_start_src + 1))

    part_type=$(echo "$line" | awk '{print $6}')

    # Pass part name with `-c "$part_num:$part_name"`
    # part_name=$(echo "$line" | awk '{print $7}')

    # Calculate partition end sector for image_dst
    part_end=$((part_start + part_size_src - 1))

    # Create the partition
    sgdisk -n "$part_num:$part_start:$part_end" -t "$part_num:$part_type" \
      "$image_dst"

    # Copy partition raw content
    dd if="$image_src" of="$image_dst" \
      bs="$sector_size" \
      count="$part_size_src" \
      skip="$part_start_src" \
      seek="$part_start" \
      conv=notrunc status=progress

    part_start=$((part_end + 1))
  fi
done <<< "$partinfo"

# vim:set ts=2 sw=2 et:
