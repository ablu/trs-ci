import glob
import os
import shutil
import requests_cache

from unittest import TestCase
from unittest.mock import MagicMock, Mock, patch
from blueprints_ci import (
    generate_tuxsuite_kas_plan,
    generate_tuxsuite_repo_plan,
    resolve_os_or_firmware,
    resolve_test_plan,
    submit_to_tuxsuite,
    find_next_stage_job_id,
    generate_lava_job_definition,
    get_local_manifest_xml_from_trs_manifest,
    get_merge_request_dependencies,
)

requests_cache.install_cache('test_utils_cache')

url = "http://some-repo"


def expected_plan():
    return {
        "jobs": [
            {
                "bake": {
                    "sources": {
                        "kas": {
                            "url": url,
                            "yaml": "kas.yml",
                        }
                    }
                }
            }
        ],
        "version": 1,
        "description": "",
        "name": None,
    }


class TestUtils(TestCase):

    def sample_settings(self):
        return Mock(
            IMAGES_DIR="images",
            NIGHTLYBUILDS_URL="http://nightly-builds-url.example",
            BLUEPRINTSCI_URL="http://blueprints-ci-url.example",
            LAVA_JOB_NAME=None,
            CI_JOB_NAME="ci-test-utils",
            CI_PROJECT_NAME="ci",
            CI_PROJECT_URL="https://gitlab.com/Linaro/blueprints/ci",
            CI_PIPELINE_ID="42",
            SKIP=None,
            TESTS=None,
            TARGET="some-target",
            TRS_MANIFEST_URL="https://gitlab.com/Linaro/trusted-reference-stack/trs-manifest/",
            IS_TRS_MANIFEST_REPO=None,
            IS_MERGE_REQUEST=False,
            ARTIFACTS=None,
            get_artifacts=dict,
        )

    def test_generate_tuxsuite_kas_plan(self):
        # Test with a single yml
        plan = generate_tuxsuite_kas_plan(url, "kas.yml")
        single_kas = expected_plan()
        self.assertEqual(single_kas, plan)

        # Test with many yml
        many_yml = expected_plan()
        many_yml["jobs"][0]["bake"]["sources"]["kas"]["yaml"] = "kas.yml:build.yml"
        plan = generate_tuxsuite_kas_plan(url, ["kas.yml", "build.yml"])
        self.assertEqual(many_yml, plan)

        # Test with ref
        ref = "1.0"
        ref_plan = expected_plan()
        ref_plan["jobs"][0]["bake"]["sources"]["kas"]["ref"] = f"refs/tags/{ref}"
        plan = generate_tuxsuite_kas_plan(url, "kas.yml", ref=ref)
        self.assertEqual(ref_plan, plan)

        # Test with branch
        branch = "kirkstone"
        branch_plan = expected_plan()
        branch_plan["jobs"][0]["bake"]["sources"]["kas"]["branch"] = branch
        plan = generate_tuxsuite_kas_plan(url, "kas.yml", branch=branch)
        self.assertEqual(branch_plan, plan)

    def test_generate_tuxsuite_repo_plan(self):
        settings = self.sample_settings()
        settings.EXTRACONFIGS = "config1,config2"
        settings.TEMPLATECONF = "template-conf"

        plan = generate_tuxsuite_repo_plan(settings)
        bake = plan["jobs"][0]["bake"]
        self.assertEqual(settings.CI_JOB_NAME, plan["name"])
        self.assertEqual("poky", bake["distro"])
        self.assertEqual("poky/oe-init-build-env", bake["envsetup"])
        self.assertEqual(settings.TARGET, bake["machine"])
        self.assertIn(settings.TARGET, bake["target"])
        self.assertEqual(["config1", "config2"], bake["extraconfigs"])
        self.assertEqual({"TEMPLATECONF": "template-conf"}, bake["environment"])
        self.assertEqual("main", bake["sources"]["repo"]["branch"])
        self.assertEqual("default.xml", bake["sources"]["repo"]["manifest"])
        self.assertEqual(settings.TRS_MANIFEST_URL, bake["sources"]["repo"]["url"])

        # Check extra artifacts
        settings.get_artifacts = lambda: {"artifact1": "path/artifact1", "artifact2": "path/artifact2"}
        plan = generate_tuxsuite_repo_plan(settings)
        bake = plan["jobs"][0]["bake"]
        self.assertEqual(["tmp_some-target/deploy/images/some-target", "path/artifact1", "path/artifact2"], bake["artifacts"])

        # Make sure repo branch or url remains the same even if running on trs-manifest repo
        settings.IS_TRS_MANIFEST_REPO = 1
        plan = generate_tuxsuite_repo_plan(settings)
        bake = plan["jobs"][0]["bake"]
        self.assertEqual(settings.CI_JOB_NAME, plan["name"])
        self.assertEqual("poky", bake["distro"])
        self.assertEqual("poky/oe-init-build-env", bake["envsetup"])
        self.assertEqual(settings.TARGET, bake["machine"])
        self.assertIn(settings.TARGET, bake["target"])
        self.assertEqual(["config1", "config2"], bake["extraconfigs"])
        self.assertEqual({"TEMPLATECONF": "template-conf"}, bake["environment"])
        self.assertEqual("main", bake["sources"]["repo"]["branch"])
        self.assertEqual("default.xml", bake["sources"]["repo"]["manifest"])
        self.assertEqual(settings.TRS_MANIFEST_URL, bake["sources"]["repo"]["url"])

    @patch('blueprints_ci.run_cmd')
    def test_submit_to_tuxsuite(self, mock_run_cmd):
        mock_proc = MagicMock()
        mock_proc.ok = True
        mock_run_cmd.return_value = mock_proc
        settings = Mock(CI_JOB_NAME="sample-tuxsuite-job", CI_JOB_ID="123")
        results_filename = f"result-{settings.CI_JOB_NAME}-{settings.CI_JOB_ID}.json"
        shutil.copyfile("blueprints_ci_tests/sample-results.json", results_filename)

        plan = generate_tuxsuite_kas_plan("http://some-repo.com", "kas.yml")
        result = submit_to_tuxsuite(settings, plan)

        mock_run_cmd.assert_called_with(['tuxsuite', 'plan', '--json', '--json-out', 'result-sample-tuxsuite-job-123.json', 'plan-sample-tuxsuite-job-123.yml'])
        self.assertEqual(settings.TUXSUITE_DOWNLOAD_URL, "https://storage.tuxsuite.com/public/blueprints/trustedsubstrate/oebuilds/2IqFtf8UEs8YU8c4mZj3Hb1QCZH/")
        self.assertIs(result, True)
        os.unlink(results_filename)

    @patch("blueprints_ci.requests.get")
    def test_resolve_os_or_firmware(self, mock_get):
        settings = self.sample_settings()
        job_name = "sample-job-name"
        image_path = "sample-image"
        expected_url = f"{settings.NIGHTLYBUILDS_URL}/-/jobs/123/artifacts/raw/{settings.IMAGES_DIR}/{image_path}"

        response = Mock()
        response.status_code = 404
        response.headers = {"Location": expected_url}
        mock_get.return_value = response

        # Test with a plain url
        url = "http://some-url"
        self.assertEqual(url, resolve_os_or_firmware(settings, url))
        self.assertEqual(url, resolve_os_or_firmware(settings, url, base_url="not-really-used"))

        # Resolve an actual thing
        expected = resolve_os_or_firmware(settings, f"{job_name}:{image_path}")
        self.assertIsNone(expected)

        response.status_code = 302
        expected = resolve_os_or_firmware(settings, f"{job_name}:{image_path}")
        self.assertEqual(expected_url, expected)

        # Test base_url
        self.assertEqual(f"{url}/{image_path}", resolve_os_or_firmware(settings, image_path, base_url=url))

        # Test multiple images
        expected_dict = {"file1": f"{url}/file1", "file2": f"{url}/file2"}
        self.assertEqual(expected_dict, resolve_os_or_firmware(settings, "file1,file2", base_url=url))

    @patch("blueprints_ci.requests.get")
    def test_find_next_stage_job_id(self, mock_get):
        settings = self.sample_settings()
        settings.CI_JOB_NAME = 'job-name'
        settings.CI_API_V4_URL = 'https://gitlab.com/api/v4'
        settings.CI_PROJECT_ID = '1'
        settings.CI_PIPELINE_ID = '1'

        response = Mock()
        response.status_code = 200
        response.json = lambda: [{"name": "check-job-name", "id": "123"}]
        mock_get.return_value = response

        expected = find_next_stage_job_id(settings)
        self.assertEqual(expected, "123")

    def test_generate_lava_job_definition(self):
        # Test generating all possible job definitions
        devices = glob.glob("blueprints_ci/lava/templates/devices/*.yaml.jinja2")
        settings = self.sample_settings()
        ci_job_name = "sample-test"
        settings.CI_JOB_NAME = ci_job_name
        default_context = {
            "os_url": "http://os.example",
            "firmware_url": "http://firmware.example",
        }
        device_contexts = {
            "kv260": {
                "firmware_url": {
                    "ImageA.bin.gz": "http://firmware.example/ImageA.bin.gz",
                    "ImageB.bin.gz": "http://firmware.example/ImageB.bin.gz",
                },
                "os_url": default_context["os_url"],
            }
        }
        for device_filename in devices:
            settings.LAVA_DEVICE = os.path.basename(device_filename).replace(".yaml.jinja2", "")
            context = device_contexts.get(settings.LAVA_DEVICE, default_context)
            definition = generate_lava_job_definition(settings, context=context)
            self.assertIsNotNone(definition)
            self.assertIn(ci_job_name, definition)

            # for now, synquacer jobs do not use firmware_url, so skip checking there
            self.assertIn(context["os_url"], definition)
            if settings.LAVA_DEVICE not in ["ava", "synquacer"]:
                if len(context["firmware_url"]) == 1:
                    self.assertIn(context["firmware_url"], definition)
                else:
                    for url in context["firmware_url"]:
                        self.assertIn(url, definition)

    @patch("blueprints_ci.download_file", return_value=True)
    def test_get_local_manifest_xml_from_trs_manifest(self, mock_download_file):
        default_xml = "default.xml"
        shutil.copyfile("blueprints_ci_tests/sample-default.xml", default_xml)

        settings = self.sample_settings()

        # Make sure git urls being overwritten actually exist in default.xml
        local_manifest_xml = get_local_manifest_xml_from_trs_manifest(settings, overwrite={
            "http://not-really.com/a/valid/repo": "bad-revision"
        })
        self.assertIsNone(local_manifest_xml)

        # Make sure the target git url actually exist in default.xml
        local_manifest_xml = get_local_manifest_xml_from_trs_manifest(settings, overwrite={
            "https://gitlab.com/soafee/ewaol/meta-ewaol.git": ("http://unknown.remote/unknown/ci.git", "test-revision"),
        })
        self.assertIsNone(local_manifest_xml)

        # Test without overwritting
        local_manifest_xml = get_local_manifest_xml_from_trs_manifest(settings)
        self.assertIsNone(local_manifest_xml)

        # Test overwritting using different branch
        local_manifest_xml = get_local_manifest_xml_from_trs_manifest(settings, overwrite={
            "https://gitlab.com/Linaro/blueprints/ci.git": "test-revision",
        })

        with open(local_manifest_xml, "r") as fp:
            self.assertIn("test-revision", fp.read())

        # Test overwritting using different repository, but same remote
        local_manifest_xml = get_local_manifest_xml_from_trs_manifest(settings, overwrite={
            "https://gitlab.com/Linaro/trustedsubstrate/meta-ledge-secure.git": ("https://gitlab.com/my-fork/ci.git", "test-revision"),
        })

        with open(local_manifest_xml, "r") as fp:
            contents = fp.read()
            self.assertIn("test-revision", contents)
            self.assertIn("my-fork/ci", contents)

        # Test overwritting using different repository and different remote
        local_manifest_xml = get_local_manifest_xml_from_trs_manifest(settings, overwrite={
            "https://gitlab.com/Linaro/blueprints/ci.git": ("https://github.com/my-github-fork/ci.git", "test-revision"),
        })

        with open(local_manifest_xml, "r") as fp:
            contents = fp.read()
            self.assertIn("test-revision", contents)
            self.assertIn("my-github-fork/ci", contents)

        # Test overwritting using a default.xml from another url (MRs)
        overwrite_filename = "overwrite_default.xml"
        with open(overwrite_filename, "w") as fp:
            fp.write("""<?xml version="1.0" encoding="UTF-8"?>
                <manifest>
                        <remote name="local-remote" fetch="https://local-remote.example" />
                        <default remote="local-remote" revision="local-remote-default" />
                        <project path="project-with-subtags" name="project/with/subtags" revision="refs/heads/main">
                                <linkfile src="original-file" dest="symlink" />
                        </project>
                        <project path="oneliner-project" name="oneliner/project.git" revision="refs/heads/main" />
                </manifest>""")

        overwrite_url = "https://url.to/default.xml"
        local_manifest_xml = get_local_manifest_xml_from_trs_manifest(settings, overwrite_url=overwrite_url)
        mock_download_file.assert_called_with(overwrite_url, output_filename=overwrite_filename)

        with open(local_manifest_xml, "r") as fp:
            contents = fp.read()
            self.assertIn("<remove-project ", contents)
            self.assertIn("project-with-subtags", contents)
            self.assertIn("original-file", contents)
            self.assertIn("oneliner-project", contents)
            self.assertNotIn("local-remote", contents)
            self.assertNotIn("local-remote-default", contents)

        # Test overwritting using a default.xml from another url (MRs) and overwritting urls again, usually coming from MR dependencies
        local_manifest_xml = get_local_manifest_xml_from_trs_manifest(settings, overwrite_url=overwrite_url, overwrite={
            "http://local-remote.example/oneliner/project.git": ("https://gitlab.com/my-fork/project.git", "test-revision"),
        })
        mock_download_file.assert_called_with(overwrite_url, output_filename=overwrite_filename)

        with open(local_manifest_xml, "r") as fp:
            contents = fp.read()
            self.assertIn("<remove-project ", contents)
            self.assertIn("project-with-subtags", contents)
            self.assertIn("original-file", contents)
            self.assertIn("oneliner-project", contents)
            self.assertIn("test-revision", contents)
            self.assertIn("my-fork/project", contents)
            self.assertNotIn("onliner/project", contents)
            self.assertNotIn("local-remote", contents)
            self.assertNotIn("local-remote-default", contents)

        os.unlink(overwrite_filename)
        os.unlink(local_manifest_xml)
        os.unlink(default_xml)

    def test_resolve_test_plan(self):
        # Ensure that testing order is the same in all boards
        settings = self.sample_settings()

        settings.LAVA_DEVICE = "qemu"
        settings.TESTS = "xtest,smoke"
        plan = resolve_test_plan(settings)
        self.assertEqual(["smoke", "xtest"], plan)

        settings.LAVA_DEVICE = "kv260"
        settings.TESTS = "secure-boot-enabled,smoke"
        plan = resolve_test_plan(settings)
        self.assertEqual(["smoke", "secure-boot-enabled"], plan)

    @patch("blueprints_ci.requests")
    def test_get_merge_request_dependencies(self, mock_requests):
        settings = self.sample_settings()

        # Do not run if not on a merge request pipeline
        self.assertEqual({}, get_merge_request_dependencies(settings))

        settings.IS_MERGE_REQUEST = True
        settings.CI_MERGE_REQUEST_IID = "123"
        mock_response = MagicMock()

        # Test bad request to merge requests page
        expected_url = f"{settings.CI_PROJECT_URL}/-/merge_requests/{settings.CI_MERGE_REQUEST_IID}"
        mock_response.ok = False
        mock_response.text = "bad request"
        mock_requests.get.return_value = mock_response
        self.assertEqual({}, get_merge_request_dependencies(settings))
        mock_requests.get.assert_called_with(expected_url)

        # Test page without CDATA
        mock_response.ok = True
        mock_response.text = "no CDATA snippet"
        mock_requests.get.return_value = mock_response
        self.assertEqual({}, get_merge_request_dependencies(settings))

        # Test key not found
        mock_response.text = """window.gl.mrWidgetData = {"id":206719013}"""
        mock_requests.get.return_value = mock_response
        self.assertEqual({}, get_merge_request_dependencies(settings))

        # Test merge request without dependencies
        mock_response.text = """window.gl.mrWidgetData = {
            "id": 206719013,
            "blocking_merge_requests": {
                "total_count": 0,
                "hidden_count": 0,
                "visible_merge_requests": {}
            }
        }""".replace("\n", "")
        mock_requests.get.return_value = mock_response
        self.assertEqual({}, get_merge_request_dependencies(settings))

        # Test it works
        # NOTE: the actual value is much bigger, the snippet below is a brief summary for testing purposes
        expected_project_url = "/Linaro/trustedsubstrate/meta-ts"
        expected_revision = "refs/merge-requests/72/merge"
        mock_response.text = """window.gl.mrWidgetData = {
            "id": 206719013,
            "blocking_merge_requests": {
                "total_count": 1,
                "visible_merge_requests": {
                    "opened": [{
                        "id": 206724915,
                        "iid": 72,
                        "title": "support yocto master branch",
                        "state": "opened",
                        "web_url": "/Linaro/trustedsubstrate/meta-ts/-/merge_requests/72",
                        "created_at": "2023-02-20T16:18:09.509Z",
                        "merged_at": null,
                        "closed_at": null
                    }]
                }
            }
        }""".replace("\n", "")
        mock_requests.get.return_value = mock_response
        expected_dependencies = {
            expected_project_url: expected_revision,
        }
        self.assertEqual(expected_dependencies, get_merge_request_dependencies(settings))
