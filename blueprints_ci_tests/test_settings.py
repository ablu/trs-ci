from blueprints_ci import Settings


class TestSettings:
    def test_no_artifacts(self):
        s = Settings(env={})
        assert s.get_artifacts() == {}
        setattr(s, "SETTINGS", "")
        assert s.get_artifacts() == {}
        setattr(s, "SETTINGS", "  ")
        assert s.get_artifacts() == {}

    def test_one_artifact(self):
        s = Settings(env={})
        setattr(s, "ARTIFACTS", "foo:foo.img")
        assert s.get_artifacts() == {"foo": "foo.img"}

    def test_two_artifacts(self):
        s = Settings(env={})
        setattr(s, "ARTIFACTS", "foo:foo.img,bar:bar.img")
        assert s.get_artifacts() == {"foo": "foo.img", "bar": "bar.img"}
