#!/usr/bin/env python3

import sys
sys.path.append(".")

from blueprints_ci import (  # noqa
    logger,
    Settings,
    fetch_job_from_squad,
    get_test_plan,
)


#
#   Required environment variables
#
required_vars = [
    # Details to send results to SQUAD
    "SQUAD_HOST",
    "SQUAD_TOKEN",
    "SQUAD_GROUP",
    "SQUAD_PROJECT",

    # Results coming from trigger_tests stage
    "SQUAD_JOB_ID",
]


#
#   Check test implementation
#
def check(settings):
    """
        1. Fetch test results for a given SQUAD job id
    """
    right_padding = 50
    bad_test_mark = "<<<<<<<"

    jobs = settings.SQUAD_JOB_ID.split(',')
    results = set()
    for job_id in jobs:
        settings.SQUAD_JOB_ID = job_id
        testjob = fetch_job_from_squad(settings)
        if not testjob.fetched:
            logger.info("****************************************************************************************************")
            logger.info("")
            logger.info(f"       The LAVA job \"{testjob.external_url}\" ({testjob.environment}) is NOT ready yet!")
            logger.info("       As soon as that LAVA job completes, this Gitlab job will be automatically triggered.")
            logger.info("")
            logger.info("****************************************************************************************************")
            return False

        logger.info(f"******************** Showing details for {testjob.external_url} ({testjob.environment}) ************************")
        logger.info(f"Fetching results from {settings.SQUAD_HOST}/api/testjobs/{settings.SQUAD_JOB_ID}")

        logger.info(f"Job log: {testjob.external_url}")
        logger.info(f"Job device: {testjob.environment}")
        logger.info("Job timings:")
        logger.info(f"  - created at:   {testjob.created_at}")
        logger.info(f"  - started at:   {testjob.started_at}")
        logger.info(f"  - ended at:     {testjob.ended_at}")
        logger.info(f"  - fetched at:   {testjob.fetched_at}")

        if testjob.job_status != "Complete":
            logger.warning(f"Job failure: {testjob.failure}")

            # Some devices might fail due to unexpected infrastructure error
            # and these should have "bypass_failure" enabled in the device test plan
            device_key = testjob.environment
            if settings.ENABLE_XEN:
                device_key = device_key.replace("-xen", "")
            device_plan = get_test_plan()[device_key]
            if device_plan.get("bypass_failure"):
                logger.info(f"{testjob.environment} FAILED RUNNING TESTS BUT CI IS CONFIGURED TO SKIP CHECKS FOR THIS DEVICE")
                return None
            else:
                results.add("fail")

        elif len(testjob.tests) == 0:
            logger.info("Job has no tests")
            results.add("fail")
        else:
            logger.info("Job tests:")
            for test in testjob.tests:
                logger.info(f"  - {test.name:<{right_padding}}: {test.status} {bad_test_mark if test.status == 'fail' else ''}")
                results.add(test.status)

        logger.info(f"******************** End of details for {testjob.external_url} ({testjob.environment}) ************************")

    return "fail" not in results


def main():
    settings = Settings(extra=required_vars)
    if settings.missing:
        return False
    return check(settings)


if __name__ == "__main__":
    exit_codes = {
        True: 0,
        False: 1,
        None: 2,
    }
    sys.exit(exit_codes.get(main(), 0))
