import logging
import re
import os
import yaml
import json
import subprocess as sp
import sys
import requests
import xml.etree.ElementTree as ET

from squad_client.core.api import SquadApi
from squad_client.core.models import TestJob, TestRun
from squad_client.utils import getid
from jinja2 import Environment, FileSystemLoader
from pathlib import Path
from urllib.parse import urlparse


"""
    This file (+build.py and test.py) holds all complexity regarding building and testing
    blueprints. Current targets are:

    - meta-ts
    - meta-ewaol
    - meta-ewaol-machine
    - meta-ledge-secure

    These are all OpenEmbedded layers that require hours to build
    and lots of disk space (+120G) to store all sources and compiled artifacts.

    In the building stage an external service called Tuxsuite (tuxsuite.com) is
    used to actually build artifacts so we don't have to worry about
    disk space nor computing power allocation. All iteraction with this service
    is done via a Python API. Tuxsuite is responsible for hosting build
    artifacts for a couple of days and it provides URLs to access these resources.

    In the testing stage an external service hosted in https://qa-reports.linaro.org
    is used as a middle-man to send test job requests and collect results to/from a LAVA
    instance. Currently, the instance is hosted in https://ledge.validation.linaro.org.
    Only some of the build artifacts generated in the building stage can be tested
    due to hardware availability in LAVA labs.

    Both building and testing are executed in each component repository
    tree as well as inside this CI repository.
"""


#
#   Set up logging
#
logger = logging.getLogger()
logger.setLevel(os.getenv("DEBUG") and logging.DEBUG or getattr(logging, os.getenv("LOGLEVEL", "INFO")))
ch = logging.StreamHandler()
ch.setFormatter(logging.Formatter("[%(levelname)s] %(message)s"))
logger.addHandler(ch)


#
#   Read entire env into settings
#
class Settings:
    def __init__(self, env=os.environ, extra=[]):
        self.env = env

        if not self.validate(extra):
            return

        longest_string = 0
        for k, v in env.items():
            if len(k) > longest_string:
                longest_string = len(k)
            setattr(self, k, v)

        if self.DEBUG:
            longest_string += 1
            for k, v in env.items():
                print(f"{k.ljust(longest_string)}{v}")

        # Pre-compute some helpers
        self.IS_CI_HOME = self.CI_PROJECT_PATH.endswith("/ci")

        self.IS_MERGE_REQUEST = self.CI_PIPELINE_SOURCE == "merge_request_event"

    def validate(self, extra_vars=[]):
        """
            Run a minimum requirement check on env vars or other
            aspects of the environment that need to be present
        """
        required_vars = [
            # Pre-defined variables coming from Gitlab-CI
            # Gitlab's APIv4 base URL
            "CI_API_V4_URL",

            # true if the job is running for a protected reference (branch or tag), empty otherwise
            "CI_COMMIT_REF_PROTECTED",

            # The first eight characters of the commit that triggered the pipeline
            "CI_COMMIT_SHORT_SHA",

            # Job's unique ID
            "CI_JOB_ID",

            # Job's name which is defined in each ci script, e.g.: build-meta-ts-qemuarm64-secureboot
            "CI_JOB_NAME",

            # Pipeline's ID
            "CI_PIPELINE_ID",

            # Event that triggered the pipeline
            # Can be push, web, schedule, api, external, chat, webide, merge_request_event, external_pull_request_event, parent_pipeline, trigger, or pipeline
            "CI_PIPELINE_SOURCE",

            # Full path of the cloned repository in the runner file system
            "CI_PROJECT_DIR",

            # Gitlab's unique project ID
            "CI_PROJECT_ID",

            # The project namespace with the project name included
            "CI_PROJECT_PATH",

            # The project URL, e.g. https://gitlab.com/Linaro/blueprints/ci
            "CI_PROJECT_URL",

            # Last part of project path
            "CI_PROJECT_NAME",

            # The address of the GitLab Container Registry: registry.gitlab.com
            "CI_REGISTRY",

            # The address of the Gitlab server: gitlab.com
            "CI_SERVER_HOST",

            # Custom variables specified in ci scripts
            # Persistent directory where build jobs will write images
            "IMAGES_DIR",

            # Base url to the blueprints CI repository
            "BLUEPRINTSCI_URL",

            # Base url to the nightly builds repository
            "NIGHTLYBUILDS_URL",
        ] + extra_vars

        self.missing = [v for v in required_vars if getattr(self, v) is None]
        if len(self.missing):
            logger.warning(f"The following environment variables are missing: {self.missing}")
            return False

        for v in required_vars:
            logger.info(f"{v}={os.getenv(v)}")

        return True

    def __getattr__(self, name):
        return self.env.get(name)

    def get_artifacts(self):
        artifact_list = self.ARTIFACTS.split(",") if self.ARTIFACTS else []
        return dict((item.split(":") for item in artifact_list))


#
#   Generate a TuxSuite plan using kas
#   NOTE: this function is only used to build meta-ewaol-machine targets and will be deprecated
#         sometime soon
#
def generate_tuxsuite_kas_plan(url, yml, ref=None, branch=None, name=None, image=None):
    if type(yml) is list:
        yml = ":".join(yml)

    source = {
        "sources": {
            "kas": {
                "url": url,
                "yaml": yml,
            }
        },
    }

    if ref:
        source["sources"]["kas"]["ref"] = f"refs/tags/{ref}"
    elif branch:
        source["sources"]["kas"]["branch"] = branch

    if name:
        source["name"] = name

    # Ref: https://docs.tuxsuite.com/plan/specifications/
    plan = {
        "version": 1,
        "name": name,
        "description": "",
        "jobs": [
            {
                "bake": source,
            },
        ],
    }

    return plan


#
#   Generate a TuxSuite plan using repo
#
def generate_tuxsuite_repo_plan(settings):

    target = settings.TARGET or settings.DEVICE

    sources = {
        "repo": {
            "branch": "main",
            "manifest": "default.xml",
            "url": settings.TRS_MANIFEST_URL,
        },
    }

    extra_artifacts = list(settings.get_artifacts().values())

    # Ref: https://docs.tuxsuite.com/plan/specifications/
    plan = {
        "version": 1,
        "name": settings.CI_JOB_NAME,
        "description": "",
        "jobs": [{
            "bake": {
                "container": "ubuntu-22.04",
                "distro": "poky",
                "envsetup": "poky/oe-init-build-env",
                "machine": target,
                "target": f"mc:{target}:{settings.TARGET_TYPE}",
                "extraconfigs": settings.EXTRACONFIGS.split(","),
                "artifacts": [f"tmp_{target}/deploy/images/{target}"] + extra_artifacts,
                "environment": {
                    "TEMPLATECONF": settings.TEMPLATECONF,
                },
                "sources": sources,
            }
        }],
    }

    return plan


#
#   Submit a tuxsuite plan to tuxsuite.com
#
def submit_to_tuxsuite(settings, plan):
    """
        1. Parse `plan` as a temporary yaml file
        2. Submit build request to tuxsuite.com
        3. Wait for build to be ready (in the future, this will be handled by callbacks)
        4. Create a results filename based on job name and id to be used in testing stage
    """

    plan_filename = f"plan-{settings.CI_JOB_NAME}-{settings.CI_JOB_ID}.yml"
    results_filename = f"result-{settings.CI_JOB_NAME}-{settings.CI_JOB_ID}.json"
    logger.debug(f"Creating temporary file for plan ({plan_filename}) and a results file ({results_filename}) for tuxsuite to write results")
    result = False
    try:
        # Write plan to a temporary file
        plan_contents = yaml.dump(plan)
        logger.info(plan_contents)
        Path(plan_filename).write_text(plan_contents)

        # Invoke tuxsuite command and wait
        tuxsuite_cmd = [
            "tuxsuite",
            "plan",
            "--json",
            "--json-out",
            results_filename,
            plan_filename,
        ]

        if "kas:" in plan_contents:
            if os.path.exists("kas-local-config.yml"):
                tuxsuite_cmd.append("--kas-override")
                tuxsuite_cmd.append("kas-local-config.yml")

        elif os.path.exists("local-manifest.xml"):
            tuxsuite_cmd.append("--local-manifest")
            tuxsuite_cmd.append("local-manifest.xml")

        proc = run_cmd(tuxsuite_cmd)
        result = proc.ok
        logger.info(proc.out)
        logger.info(proc.err)
    except Exception as e:
        logger.warning(f"Could not submit build request to Tuxsuite: {e}")
    finally:
        os.remove(plan_filename)

    logger.info(f"Looking for `download_url` in {results_filename}")
    try:
        with open(results_filename, "r") as fp:
            json_result = json.load(fp)

        # There must exist only one build in the file
        build_id = list(json_result["builds"].keys())[0]
        download_url = json_result["builds"][build_id]["download_url"]
        settings.TUXSUITE_DOWNLOAD_URL = download_url
    except Exception as e:
        logger.warning(f"Could not find dowload_url in {results_filename}: {e}")

    return result


#
#   Download any file from URL and save it to output_filename
#
def download_file(url, output_filename=None):
    output_filename = Path(output_filename or os.path.basename(url))
    try:
        MB = 1024 ** 2
        download = requests.get(url, stream=True)
        show_progress = logger.isEnabledFor(logging.INFO)
        with output_filename.open("wb") as f:
            for chunk in download.iter_content(chunk_size=MB):
                f.write(chunk)
                if show_progress:
                    print(".", end="", flush=True)

        if show_progress:
            print(" OK")
    except requests.exceptions.HTTPError as e:
        logger.warning(f"Could not download {url}: {e}")
        return False
    return True


#
#   Download image from Tuxsuite
#
def download_image(settings, output_directory="."):
    # TODO: add unittests to this function
    """
        1. Retrieve "settings.TUXSUITE_DOWNLOAD_URL / dirname(IMAGE)"
        2. Iterate over files received in step-2
        3. If there's re.match(basename(IMAGE)), download it
        4. Remove any timestamp in the file name
    """

    image_path = settings.IMAGE
    image_dir = os.path.dirname(image_path)
    image_filename = os.path.basename(image_path)
    image_url = None

    # The image file name provided by IMAGE env var might be a regex pattern to the real image name
    # due to images being time-stamped
    image_real_name = None

    # Now attempt to find the image in build files in tuxsuite
    url = f'{settings.TUXSUITE_DOWNLOAD_URL}/{image_dir}/'
    logger.info(f"Looking for {image_filename} in {url}")
    build_files = requests.get(url)
    for candidate in build_files.json()['files']:
        candidate_basename = os.path.basename(candidate['Url'])
        if re.match(f'^{image_filename}$', candidate_basename):
            logger.info(f"Found {candidate_basename}")
            image_url = f'{url}{candidate_basename}'

            # Remove eventual timestamps from filename
            image_real_name = re.sub(r"-\d{8,}", "", candidate_basename)
            if image_real_name != candidate_basename:
                logger.info(f"Image renamed (removed timestamp): {candidate_basename} -> {image_real_name}")
            break

    if image_url is None:
        logger.warning(f"Could not retrieve URL for {image_path} within {url}")
        return False, ""

    # Really download image
    logger.info(f"Downloading {image_url}")
    output_directory = Path(output_directory)
    output_directory.mkdir(exist_ok=True)
    output_filename = output_directory / image_real_name

    download_ok = download_file(image_url, output_filename=output_filename)
    return download_ok, output_filename


#
#   Generate LAVA job definition
#
def generate_lava_job_definition(settings, context={}):
    try:
        templates_dir = ['blueprints_ci/lava/templates/']
        env = Environment(loader=FileSystemLoader(templates_dir))

        env.globals['basename'] = os.path.basename
        env.globals['splitext'] = os.path.splitext
        context['settings'] = settings

        return env.get_template(f"devices/{settings.LAVA_DEVICE}.yaml.jinja2").render(context)
    except Exception as e:
        logger.warning(f"Could not generate LAVA job definition: {e}")
        raise e


#
#   Send a result to SQUAD telling if the build passed or failed
#
def send_build_result_to_squad(settings):
    logger.info(f"Sending build result \"{settings.BUILD_RESULT}\" to {settings.SQUAD_HOST}/{settings.SQUAD_GROUP}/{settings.SQUAD_PROJECT}/build/{settings.SQUAD_BUILD}")

    Path("/tmp/metadata.json").write_text(f"""{{
        "build_url": "{settings.DEVICE} {settings.CI_PROJECT_URL}/-/jobs/{settings.CI_JOB_ID}",
        "build artifacts": "{settings.DEVICE} {settings.TUXSUITE_DOWNLOAD_URL}",
        "build log": "{settings.DEVICE} {settings.TUXSUITE_DOWNLOAD_URL}/build.log"
    }}""")

    cmd = [
        "squad-client",
        "submit",
        "--group", settings.SQUAD_GROUP,
        "--project", settings.SQUAD_PROJECT,
        "--build", settings.SQUAD_BUILD,
        "--environment", settings.DEVICE,
        "--result-name", "build/build",
        "--result-value", settings.BUILD_RESULT,
        "--metadata", "/tmp/metadata.json",
    ]

    proc = run_cmd(cmd)
    return proc.ok


#
#   Send a job definition to squad, that will send to LAVA
#
def send_testjob_request_to_squad(settings, job_definition):
    with open("/tmp/definition.yml", "w") as fp:
        fp.write(job_definition)

    environment = settings.LAVA_DEVICE
    if settings.ENABLE_XEN:
        environment += "-xen"

    cmd = [
        "squad-client",
        "submit-job",
        "--group", settings.SQUAD_GROUP,
        "--project", settings.SQUAD_PROJECT,
        "--build", settings.SQUAD_BUILD,
        "--backend", "ledge.validation.linaro.org",
        "--environment", environment,
        "--definition", "/tmp/definition.yml",
    ]

    proc = run_cmd(cmd)
    if not proc.ok:
        logger.warning(f"Could not submit job: {proc.out}, {proc.err}")
        return False, None

    logger.info(f"See test job progress at {settings.SQUAD_HOST}/{settings.SQUAD_GROUP}/{settings.SQUAD_PROJECT}/build/{settings.SQUAD_BUILD}/testjobs/")

    matches = re.findall(r"SQUAD job id: (\d+)", proc.out + proc.err)
    if len(matches) != 1:
        logger.warning(f"Could not obtain SQUAD job id: match returned {matches}")
        return False, None

    job_id = matches[0]
    return True, job_id


#
#   Register the next job's URL trigger to SQUAD build
#   meaning that whenever the SQUAD detects that the build finished
#   it'll make an HTTP POST request to the Gitlab job URL, thus triggering
#   the "check" job, which will fetch test results accordingly
#
def register_callback_in_squad(settings):
    job_id = find_next_stage_job_id(settings)
    check_stage_job_url = f"{settings.CI_API_V4_URL}/projects/{settings.CI_PROJECT_ID}/jobs/{job_id}/play"

    cmd = [
        "squad-client",
        "register-callback",
        "--group", settings.SQUAD_GROUP,
        "--project", settings.SQUAD_PROJECT,
        "--build", settings.SQUAD_BUILD,
        "--url", check_stage_job_url,
    ]

    proc = run_cmd(cmd)
    return proc.ok


#
#   Use settings.SQUAD_JOB_ID to fetch test results from a JOB in LAVA
#   referenced in SQUAD.
#
def fetch_job_from_squad(settings):

    SquadApi.configure(url=settings.SQUAD_HOST)

    testjob = TestJob(settings.SQUAD_JOB_ID)
    if not testjob.fetched:
        return testjob

    # Some jobs might fail due to LAVA infra errors. SQUAD detects that and
    # resubmits them automatically. The snippet bellow attemps to find a good
    # resubmitted job
    if testjob.job_status != "Complete":
        resubmitted_jobs = testjob.resubmitted_jobs()
        if len(resubmitted_jobs) > 0:
            logger.info(f"Job {testjob.external_url} had an infrastructure error ({testjob.failure}). SQUAD automatically tried to resubmit it or it was resubmitted manually.")
            good_job = None
            for job_id, job in resubmitted_jobs.items():
                if job.job_status == "Complete":
                    good_job = job
                    break

            if good_job is not None:
                logger.info(f"    Found good resubmitted job {job.external_url}")
                testjob = good_job
            else:
                logger.info("     No good resubmitted job found. This can mean 2 things:")
                logger.info("         a) SQUAD didn't resubmit the offending job at all, or")
                logger.info("         b) All attempts to resubmit the job resulted in failure")

    testjob.tests = []
    if testjob.testrun:
        testrun = TestRun(getid(testjob.testrun))
        tests = testrun.tests(fields="id,name,status").values()
        testjob.tests = sorted(tests, key=lambda t: t.name)

        testjob.log = None
        log_filename = f"job-{settings.SQUAD_JOB_ID}.log"
        if download_file(testrun.log_file, output_filename=log_filename):
            with open(log_filename, "r") as fp:
                testjob.log = fp.read()

    return testjob


#
#   Find the Gitlab's job id for the job in the next
#   stage after "trigger_tests" job.
#
def find_next_stage_job_id(settings):
    job_name = f"check-{settings.CI_JOB_NAME}"
    pipeline_jobs_url = f"{settings.CI_API_V4_URL}/projects/{settings.CI_PROJECT_ID}/pipelines/{settings.CI_PIPELINE_ID}/jobs"

    response = requests.get(pipeline_jobs_url)
    if not response.ok:
        logger.warning(f"Could not retrieve id for \"{job_name}\": {response.text}")
        return None

    for job in response.json():
        if job["name"] == job_name:
            return job["id"]

    logger.warning(f"Could not retrieve id for \"{job_name}\" in {response.json()}")
    return None


#
#   Resolve OS or FIRMWARE url
#
def resolve_os_or_firmware(settings, spec, base_url=None):
    """
        Resolve `spec` into an actuall URL or set of URLs

        If `base_url` is None, the function will try to look up for
        jobs in the nightly pipelines that contain specified files.
        In this case, `spec` will be in the format like "job-name:file-name".

        If `base_url` is present, then this function will simply append
        `spec` to it.

        NOTE: `spec` might define multiple images separated by ','. In such cases
              this function will return a dictionary with `basename(image)` for keys
              and full URL for values. For instance, if spec is "file1,file2" and base_url is "my-url",
              the function will return a dictinary like

              ```
              {
                  "file1": "my-url/file1",
                  "file2": "my-url/file2",
              }
              ```
    """

    if spec.startswith("http"):
        return spec

    if ':' in spec:
        urls = {}
        job_name, images_path = spec.split(":")
        for image_path in images_path.split(","):
            # The URL gitlab provides needs 1 redirection to the to the actual image URL, which contains the image
            # as basename of the url, so we need to get that final location
            image_url = f"{settings.NIGHTLYBUILDS_URL}/-/jobs/artifacts/main/raw/{settings.IMAGES_DIR}/{image_path}?job={job_name}"
            logger.info(f"Resolving {spec}: GET {image_url}")
            urls[image_path] = resolve_redirect(image_url)

        return urls if len(urls) > 1 else list(urls.values())[0]

    if base_url:
        images = spec.split(',')
        if len(images) == 1:
            return f"{base_url}/{spec}"
        return {image: f"{base_url}/{image}" for image in images}

    logger.warning("Could not resolve os or firmware")
    return None


def resolve_redirect(url):
    response = requests.get(url, allow_redirects=False)
    if response.status_code != 302:
        return None
    return response.headers["Location"]


def resolve_artifact(settings, spec, base_url=None):
    if base_url:
        filename = os.path.basename(spec)
        return f"{base_url}/{filename}"

    job_name, filename = spec.split(":")
    url = f"{settings.NIGHTLYBUILDS_URL}/-/jobs/artifacts/main/raw/{settings.ARTIFACTS_DIR}/{filename}?job={job_name}"
    logger.info(f"Resolving {spec}: GET {url}")
    return resolve_redirect(url)


#
#   Read test_plan.yml and return a dictionary
#
def get_test_plan():
    with open("blueprints_ci/test_plan.yml", "r") as fp:
        try:
            test_plan = yaml.safe_load(fp)
        except yaml.YAMLError as e:
            logger.warning(f"Cannot load test_plan.yml: {e}")
            return None
    return test_plan


#
#    Resolve test plan
#
def resolve_test_plan(settings):
    """
        Read the default test plan file (blueprints_ci/test_plan.yml)
        and check if there are any extra changes specified in GitlabCI job.
        Then return a blob of file with all tests combined
    """
    test_plan = get_test_plan()
    all_tests = test_plan["tests"]
    device_plan = test_plan[settings.LAVA_DEVICE]
    skip = settings.SKIP or (device_plan.get("skip") if device_plan else None)
    tests = settings.TESTS or (device_plan.get("tests") if device_plan else None)
    nightly = device_plan.get("nightly") if device_plan else None

    if skip:
        to_skip = skip.split(',') if type(skip) is str else skip
        plan = list(set(all_tests) - set(to_skip))
    elif tests:
        plan = tests.split(',') if type(tests) is str else tests
        if nightly and settings.RUNNING_NIGHTLY:
            plan += nightly
    else:
        plan = all_tests

    ordered_plan = []
    for test in all_tests:
        if test in plan:
            ordered_plan.append(test)

    return ordered_plan


#
#    Download default.xml from TRS-manifests and generate a local-manifest.xml overwritting git urls accordingly
#
def get_local_manifest_xml_from_trs_manifest(settings, overwrite={}, overwrite_url=None):
    """
        1. Download default.xml from TRS_MANIFEST_URL
        2. Generate a local_manifest.xml using 2 different approaches:

            either overwrite just git URLs from `overwrite` (2.1) or overwrite the entire default.xml using `overwrite_url` (2.2)

        2.1. Overwrite git urls with `overwrite` parameter
            2.1.1. There might be 2 scenarios
             a) git url is the same as the one in default.xml, just changing the revision
             b) git url is different. This might be the case of a merge request coming from
                a forked repository. Example:

                Bob forked Linaro/trustedsubstrate/meta-ts into his own repository Bob/meta-ts.
                Then this function will find the <project /> tag containing Linaro/trustedsubstrate/meta-ts and
                replace that with Bob/meta-ts. There is one caveat though: Bob's tree might be a different
                git service. While Linaro's in gitlab.com, Bob's could be in github.com or yocto.org, or anything else.

                This function takes care of replacing the remote as well.

        2.2. Overwrite the entire default.xml by using `overwrite_url`. This is the approach when running CI on trs-manifest
             repository. Changes there can modify not only git urls but also add/remove projects/remotes or anything else.

             NOTE: The repo tool doesn't allow editing anything but projects via local_manifest.xml, so we are limited to that
                   feature. If there is any change in trs-manifest/default.xml besides project tags, manual actions will be
                   required.

             The local_manifest.xml is generated by removing all original projects in default.xml using the `<remove />` tag, and later
             blindly pasting all project tags of default.xml from `overwrite_url`.

        3. Sample of local-manifest.xml

             <?xml version="1.0" encoding="UTF-8"?>
             <manifest>
                     <remove-project name="git/meta-selinux" />
                     <project path="meta-selinux"        name="git/meta-selinux"            revision="refs/heads/master"       remote="yocto" />
             </manifest>

        4. Transform default.xml into local-manifest.xml by removing all remotes and all projects that didn't get changed
    """

    filename = "default.xml"
    if not download_file(f"{settings.TRS_MANIFEST_URL}-/raw/main/{filename}"):
        return None

    if overwrite_url is None and len(overwrite) == 0:
        # No need for local-manifest.xml
        return None

    default_xml = ET.parse(filename)
    root = default_xml.getroot()
    projects = {p.attrib["name"].replace(".git", "").lower(): p for p in root.findall("project")}
    remotes = {r.attrib["name"]: r for r in root.findall("remote")}
    default_remote = root.findall("default")[0]
    default_remote_name = default_remote.attrib["remote"]
    remotes[None] = remotes[default_remote_name]
    remove_projects = {}  # project name => project tag

    if overwrite_url:
        overwrite_filename = "overwrite_default.xml"
        if not download_file(overwrite_url, output_filename=overwrite_filename):
            logger.warning(f"Could not retrieve {filename} from \"{overwrite_url}\"")
            return None

        # Add `<remove-project />` for all existing projects
        for project in projects.values():
            name = project.attrib["name"]
            remove_projects[name] = ET.Element("remove-project", attrib={"name": name})

        # Remove original projects
        try:
            for project in projects.values():
                root.remove(project)
        except ValueError as e:
            logger.warning(f"UNEXPECTED BEHAVIOR removing {project}({project.attrib}): {e}")
            return None

        # Blindly paste everything from `overwrite_url`
        overwrite_default_xml = ET.parse(overwrite_filename)
        overwrite_projects = overwrite_default_xml.getroot().findall("project")
        for tag in overwrite_projects:
            tag.tail = "\n\t"
            root.insert(0, tag)

        # Overwrite projects variable so that eventual MR dependencies can overwrite urls again
        projects = {p.attrib["name"].replace(".git", "").lower(): p for p in root.findall("project")}

    for git_url, revision in overwrite.items():
        target_git_url = None
        if type(revision) is tuple:
            target_git_url, revision = revision

        # Get path without the leading forward slash, without .git and all lowers
        path = urlparse(git_url).path[1:].replace(".git", "").lower()

        if path not in projects:
            logger.warning(f"UNEXPECTED: input git tree {path} DOES NOT EXIST IN {settings.TRS_MANIFEST_URL}: {list(projects.keys())}")
            return None

        project = projects.pop(path)
        name = project.attrib["name"]
        remove_projects[name] = ET.Element("remove-project", attrib={"name": name})

        project.attrib["revision"] = revision

        if target_git_url:
            url = urlparse(target_git_url)

            # Set new name attribute
            project.attrib["name"] = url.path[1:].replace(".git", "").lower()

            # Check if remote needs changing
            new_remote = None
            for remote in remotes.values():
                if f"{url.scheme}://{url.netloc}" == remote.attrib["fetch"]:
                    new_remote = remote
                    break

            if new_remote is None:
                logger.warning(f"The target git url \"{target_git_url}\" points to a remote location not known by default.xml: {list(remotes.keys())}")
                return None

            current_remote = remotes[project.attrib.get("remote")]
            if new_remote != current_remote:
                project.attrib["remote"] = new_remote.attrib["name"]

    # Transform default.xml into local-manifest.xml
    del remotes[None]
    tags_to_remove = [default_remote] + list(remotes.values())
    if overwrite_url is None:
        tags_to_remove += list(projects.values())

    try:
        for tag in tags_to_remove:
            root.remove(tag)
    except ValueError as e:
        logger.warning(f"UNEXPECTED BEHAVIOR removing {tag}({tag.attrib}): {e}")
        return None

    for tag in remove_projects.values():
        tag.tail = "\n\t"
        root.insert(0, tag)

    local_manifest_xml = "local-manifest.xml"
    default_xml.write(local_manifest_xml, encoding="utf-8", xml_declaration=True)
    return local_manifest_xml


def print_tuxsuite_build_log(settings, file=sys.stdout, logs=["build.log"]):
    """
        The build log is stored (if any) in settings.TUXSUITE_DOWNLOAD_URL + "/build.log",
        which is retrieved within `download_image()` function.
    """

    if settings.PRINT_BUILD_LOG != "true":
        return

    for filename in logs:
        log_url = f"{settings.TUXSUITE_DOWNLOAD_URL}{filename}"
        logger.info(f"Printing error messages from {log_url}:")
        download_ok = download_file(log_url, output_filename=filename)
        got_errors = False
        if download_ok:
            lineno = 0
            with open(filename, "r") as fp:
                for line in fp.readlines():
                    lineno += 1
                    if line.lower().startswith("error:"):
                        logger.info(f"- {lineno} {line}")
                        got_errors = True
            if not got_errors:
                logger.info("No errors detected")
        else:
            logger.warning(f"Could not retrieve {log_url}")


def run_cmd(cmd):
    """
        Run the specified `cmd` and waits for its completion.
        Returns `proc` with extra attributes:
        - `ok` True if the the command returned 0, False otherwise
        - `out` decoded command stdout
        - `err` decoded command stderr
    """
    logger.info(f"Running {cmd}")
    proc = sp.Popen(cmd, stdout=sp.PIPE, stderr=sp.PIPE)
    stdout, stderr = proc.communicate()
    proc.ok = proc.returncode == 0

    proc.out = stdout.decode()
    proc.err = stderr.decode()
    return proc


def get_merge_request_dependencies(settings):
    """
        Check Gitlab's merge request page for current merge request if there are any dependencies,
        and if so, get their URLs inside the local-manifest.xml to pass to the build
        definition of the current merge request.

        1. Get dependencies
        1.1. Download the merge request page
        1.2. Look for the string "window.gl.mrWidgetData = {"
        1.3. Parse only that line from json
        1.4. Dependencies will be listed in the key ["blocking_merge_requests"]["visible_merge_requests"]["opened"]
        2. For each dependency
        2.1. Get the original project url and its refs/merge-requests/<iid>/merge
        3. Return a dictionary in the form:
        return {
            "<project-url>": "refs/merge-requests/<iid>/merge",
            ...
        }

        NOTE: unfortunately Gitlab does not offer a formal REST API to get merge request
              dependencies [1], so we have to brute-force get it. The brute-force is based
              on retrieving json CDATA snippet that is given to the web page as non-ajax data,
              so basically is json data loaded after page's html.

              This isn't optimal as Gitlab might change this behavior at any time, so when the API
              becomes available, we should do the switch.

        [1] https://docs.gitlab.com/ee/user/project/merge_requests/dependencies.html#api-support-for-managing-merge-request-dependencies
    """

    if not settings.IS_MERGE_REQUEST:
        logger.warning("Getting merge request dependencies can only be run on a merge request pipeline")
        return {}

    dependencies = {}
    merge_request_url = f"{settings.CI_PROJECT_URL}/-/merge_requests/{settings.CI_MERGE_REQUEST_IID}"

    response = requests.get(merge_request_url)
    if not response.ok:
        logger.warning(f"Could not retrieve merge request details from \"{merge_request_url}\": {response.text}")
        return {}

    cdata_line = None
    for line in response.text.split("\n"):
        if line.startswith("window.gl.mrWidgetData = {"):
            cdata_line = line
            break

    if cdata_line is None:
        logger.info("No CDATA detected, Gitlab probably has changed!")
        return {}

    json_data = json.loads(cdata_line.replace("window.gl.mrWidgetData = ", ""))
    try:
        if int(json_data["blocking_merge_requests"]["total_count"]) == 0:
            logger.info("No merge request dependencies detected")
            return {}

        mr_dependencies = json_data["blocking_merge_requests"]["visible_merge_requests"]["opened"]
        logger.info(f"Found {len(mr_dependencies)} dependencies:")
        for dependency in mr_dependencies:
            logger.info(f"- {dependency['web_url']}: {dependency['title']}")
            project_url, _ = dependency["web_url"].split("/-/")
            iid = dependency["iid"]
            if project_url in dependencies:
                logger.warning(f"Project {project_url} is present in more than one dependency and that is not supported, aborting handling merge request dependencies")
                return {}

            dependencies[project_url] = f"refs/merge-requests/{iid}/merge"
    except KeyError as e:
        logger.warning(f"Could not parse dependencies, missing key: {e}")
        return {}

    return dependencies
