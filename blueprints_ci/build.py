#!/usr/bin/env python3
from datetime import date

import sys

import os
import shutil
import subprocess as sp

from pathlib import Path

sys.path.append(".")
from blueprints_ci import (  # noqa
    logger,
    Settings,
    generate_tuxsuite_kas_plan,
    generate_tuxsuite_repo_plan,
    submit_to_tuxsuite,
    download_image,
    download_file,
    send_build_result_to_squad,
    print_tuxsuite_build_log,
)


#
#   Required environment variables
#
required_vars = [
    # Basic configuration to send build requests to tuxsuite.com
    "TUXSUITE_GROUP",
    "TUXSUITE_PROJECT",

    # Details to send results to SQUAD
    "SQUAD_HOST",
    "SQUAD_TOKEN",
    "SQUAD_GROUP",
    "SQUAD_PROJECT",

    # Token used when running Merge Request jobs
    # the TUXSUITE_TOKEN is only available to protected branches
    "TUXSUITE_TOKEN_MR",

    # The image should be specified by the ci job definition, it'll be downloaded afterwards
    "IMAGE",

    # Which device the build job is building to
    "DEVICE",

    # URL for default.xml, TRS's manifest file
    "TRS_MANIFEST_URL",
]


def write_buildenv(settings, buildenv):
    with open(f"{settings.CI_PROJECT_DIR}/build.env", "w") as fp:
        for env, value in buildenv.items():
            fp.write(f"{env}={value}\n")


#
#   Build implementation
#
def build(settings):
    """
        1. Generate tuxsuite plan (dict)
        2. Call tuxsuite on generated plan
        2. Download important artifacts
        3. Write useful information to ${CI_PROJECT_DIR}/build.env to pass to testing stage
          - RESULT_IMAGE_FILE: full path of the image
    """

    # When running nightly builds/tests, LAVA tests will be defined in nightly-builds.yml
    settings.SQUAD_BUILD = settings.CI_COMMIT_SHORT_SHA
    if settings.RUNNING_NIGHTLY:
        settings.SQUAD_BUILD = str(date.today())

    # Merge Request pipelines don't have TUXSUITE_TOKEN, they need to use the *_MR one
    if settings.IS_MERGE_REQUEST:
        os.environ["TUXSUITE_TOKEN"] = settings.TUXSUITE_TOKEN_MR
        settings.SQUAD_BUILD = f"mr-{settings.CI_MERGE_REQUEST_IID}-{settings.SQUAD_BUILD}"

    if settings.TUXSUITE_TOKEN is None:
        logger.warning("The following environment variables are missing: ['TUXSUITE_TOKEN']")
        return False

    # To be written to build.env
    buildenv_out = {
        "DEVICE": settings.DEVICE,
        "TARGET": settings.TARGET,
        "SQUAD_HOST": settings.SQUAD_HOST,
        "SQUAD_TOKEN": settings.SQUAD_TOKEN,
        "SQUAD_GROUP": settings.SQUAD_GROUP,
        "SQUAD_PROJECT": settings.SQUAD_PROJECT,
        "SQUAD_BUILD": settings.SQUAD_BUILD,
    }

    if settings.KAS_YAML:
        # TODO: KAS builds are going to be deprecated sometime soon,
        # all git branch/ref resolution will be handled in manifest.py
        # Change git url and branch/tag when running merge requests

        # FIXME: for some reason, GIT_URL defined in project settings take higher priority than the one in meta-ewaol-machine.yml
        #        so we need to hardcode this for kas plans
        git_url = "https://gitlab.com/soafee/ewaol/meta-ewaol-machine.git"
        git_branch = settings.GIT_BRANCH
        git_ref = settings.GIT_REF
        if settings.IS_MERGE_REQUEST and settings.CI_PROJECT_NAME == "meta-ewaol-machine":
            git_url = settings.CI_MERGE_REQUEST_SOURCE_PROJECT_URL
            git_branch = settings.CI_MERGE_REQUEST_SOURCE_BRANCH_NAME
            if git_url is None or git_branch is None:
                logger.warning("The following environment variables are missing: ['CI_MERGE_REQUEST_SOURCE_PROJECT_URL', 'CI_MERGE_REQUEST_SOURCE_BRANCH_NAME']")
                write_buildenv(settings, buildenv_out)
                return False

        logger.info(f"Generating tuxsuite plan for {git_url} (branch: {git_branch} or ref: {git_ref}) and kas file(s) {settings.KAS_YAML}")
        tuxsuite_plan = generate_tuxsuite_kas_plan(
            git_url,
            settings.KAS_YAML,
            ref=git_ref,
            branch=git_branch,
            name=settings.CI_JOB_NAME,
            image=settings.IMAGE,
        )
    else:

        if settings.TEMPLATECONF is None or settings.EXTRACONFIGS is None:
            logger.warning("The following environment variables are missing: ['TEMPLATECONF', 'EXTRACONFIGS']")
            write_buildenv(settings, buildenv_out)
            return False

        if not settings.TARGET_TYPE:
            logger.info("Generating tuxsuite plan")
            logger.warning("The following environment variables are missing: ['TARGET_TYPE']")
            write_buildenv(settings, buildenv_out)
            return False

        tuxsuite_plan = generate_tuxsuite_repo_plan(settings)

    logger.info("Submitting plan to tuxsuite")
    result = submit_to_tuxsuite(settings, tuxsuite_plan)

    print_tuxsuite_build_log(settings)

    if not result:
        print_tuxsuite_build_log(settings, logs=["fetch.log", "bitbake-environment"])
        write_buildenv(settings, buildenv_out)
        return False

    images_dir = Path(settings.IMAGES_DIR)
    images_dir.mkdir(exist_ok=True)
    artifacts_dir = Path(settings.ARTIFACTS_DIR)
    artifacts_dir.mkdir(exist_ok=True)

    images = settings.IMAGE.split(',')
    result_images = []
    for image in images:
        settings.IMAGE = image
        logger.info(f"Downloading image {settings.IMAGE}")
        result, image_path = download_image(settings)
        if not result:
            write_buildenv(settings, buildenv_out)
            return False

        # Some images might not be compressed
        _, extension = os.path.splitext(image_path)
        if extension not in [".gz", ".bz2", ".zip", ".xz"]:
            logger.info(f"Image {image_path} does not seem compressed. Compressing it with gzip...")
            proc = sp.Popen(["gzip", image_path])
            proc.wait()
            if proc.returncode != 0:
                return False
            image_path = Path(str(image_path) + ".gz")

        logger.info(f"Placing {image_path} in {settings.IMAGES_DIR}")
        shutil.copy(image_path, images_dir)
        image_path.unlink()
        result_images.append(os.path.basename(image_path))

    # TODO: find a better way to determine if this builds a firmware or an os
    if "meta-ts" in settings.CI_JOB_NAME:
        buildenv_out["FIRMWARE_BUILD_JOB_ID"] = settings.CI_JOB_ID
        buildenv_out["FIRMWARE_RESULT_IMAGE_FILE"] = ','.join(result_images)
        buildenv_out["FIRMWARE_ARTIFACTS"] = settings.ARTIFACTS if settings.ARTIFACTS else ""
    else:
        buildenv_out["OS_BUILD_JOB_ID"] = settings.CI_JOB_ID
        buildenv_out["OS_RESULT_IMAGE_FILE"] = ','.join(result_images)
        buildenv_out["OS_ARTIFACTS"] = settings.ARTIFACTS if settings.ARTIFACTS else ""

    write_buildenv(settings, buildenv_out)

    artifacts = settings.get_artifacts().values()
    for artifact in artifacts:
        logger.info(f"Downloading artifact \"{artifact}\"")
        artifact = os.path.basename(artifact)
        if download_file(f"{settings.TUXSUITE_DOWNLOAD_URL}/{artifact}"):
            shutil.copy(artifact, artifacts_dir)

    return True


def main():
    settings = Settings(extra=required_vars)
    if settings.missing:
        return False

    # Let SQUAD aware of this build's result
    build_result = build(settings)
    settings.BUILD_RESULT = "pass" if build_result else "fail"
    send_build_result_to_squad(settings)

    return build_result


if __name__ == "__main__":
    sys.exit(0 if main() else 1)
