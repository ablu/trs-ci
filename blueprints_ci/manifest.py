#!/usr/bin/env python3
import sys

sys.path.append(".")
from blueprints_ci import (  # noqa
    logger,
    Settings,
    get_local_manifest_xml_from_trs_manifest,
    get_merge_request_dependencies,
)


#
#   Required environment variables
#
required_vars = [
    # Variables coming from ci scripts (meta-ts.yml, meta-ewaol-machine.yml, ...)
    "GIT_URL",

    # URL for default.xml, TRS's manifest file
    "TRS_MANIFEST_URL",
]


#
#   Build implementation
#
def manifest(settings):
    """
        1. Take in components git urls
        2. Download default.xml from TRS_MANIFEST_URL
        3. Overwrite default.xml using git urls retrieved on step 1
        4. Generate a local-manifest.xml from the differences on step 3
    """

    # Change git url and branch/tag when running merge requests
    git_branch = settings.GIT_BRANCH
    git_ref = settings.GIT_REF
    git_urls = {
        settings.GIT_URL: git_branch or git_ref,
    }
    if settings.IS_MERGE_REQUEST:
        if settings.CI_MERGE_REQUEST_IID is None:
            logger.warning("The following environment variables are missing: ['CI_MERGE_REQUEST_IID']")
            return False

        # This is supported by Gitlab and should contain MR's patch on top of target branch
        # Ref: https://docs.gitlab.com/ee/user/project/merge_requests/reviews/#checkout-merge-requests-locally-through-the-head-ref
        git_urls[settings.GIT_URL] = f"refs/merge-requests/{settings.CI_MERGE_REQUEST_IID}/merge"

        # Check if current merge request depends on other merge requests
        merge_request_dependencies = get_merge_request_dependencies(settings)
        for url, rev in merge_request_dependencies.items():
            if url == settings.GIT_URL:
                logger.warning("Merge request dependency cannot depend on the same project, \"{rev}\" will be ignored!")
                continue
            git_urls[url] = rev

    overwrite_url = None
    if settings.IS_TRS_MANIFEST_REPO:
        # Ref can be either
        # CI_COMMIT_TAG: only present in tag pipelines, which are triggered when a new tag is pushed
        # git_branch: which can be one of the two
        #   1. defaults to project GIT_URL, or
        #   2. the Merge Request's fork git url, if it's a merge request pipeline
        #
        # NOTE: When changing default.xml, it is still better to generate a local_manifest.xml with changes in it
        #       instead of modifying repository url in TuxSuite's build definition. The reason for this is caching.
        #       TuxSuite caching mechanism is done by using the repo manifest url and branch as key
        #       so, changing those mean building on a clean cache, taking many many hours (~3h).
        #
        # NOTE 2: There is one limitiation with creating local_manifest.xml though. It can only make changes to project tags.
        #         Therefore, if the change in default.xml adds a new remote, CI just will not pick that. This is due to repo tool.

        overwrite_trs_manifest_url = settings.CI_MERGE_REQUEST_SOURCE_PROJECT_URL or settings.GIT_URL
        overwrite_trs_manifest_branch = settings.CI_COMMIT_TAG or settings.CI_MERGE_REQUEST_SOURCE_BRANCH_NAME or git_branch
        overwrite_url = f"{overwrite_trs_manifest_url}/-/raw/{overwrite_trs_manifest_branch}/default.xml"
        del git_urls[settings.GIT_URL]

    local_manifest_xml_filename = get_local_manifest_xml_from_trs_manifest(settings, overwrite=git_urls, overwrite_url=overwrite_url)
    if local_manifest_xml_filename is None:
        return False

    logger.info("local-manifest.xml:")
    with open(local_manifest_xml_filename, "r") as fp:
        logger.info(fp.read())

    return True


def main():
    settings = Settings(extra=required_vars)
    if settings.missing:
        return False

    return manifest(settings)


if __name__ == "__main__":
    sys.exit(0 if main() else 1)
